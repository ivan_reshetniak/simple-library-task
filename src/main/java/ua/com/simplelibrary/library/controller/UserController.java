package ua.com.simplelibrary.library.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.com.simplelibrary.library.form.TakeForm;
import ua.com.simplelibrary.library.form.UserForm;
import ua.com.simplelibrary.library.model.User;
import ua.com.simplelibrary.library.service.UserServiceImpl;

import java.util.List;

@RestController
@RequestMapping(value = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class UserController {

    private UserServiceImpl userService;

    @PostMapping("/add")
    public User addUser(@RequestBody UserForm form) {
        return userService.addUser(form);
    }

    @GetMapping
    public List<User> getAllUsers() {
        return userService.getAll();
    }

    @DeleteMapping("/{id}")
    public void deleteUserById(@PathVariable int id) {
        userService.deleteUserById(id);
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable int id, UserForm form) {
        return userService.updateUser(id, form);
    }

    @PutMapping("/takeBook")
    public User takeBook(@RequestBody TakeForm form) {
        return userService.takeBook(form);
    }

    @PutMapping("/returnBook")
    public User returnBook(@RequestBody TakeForm form) {
        return userService.returnBook(form);
    }

    @GetMapping("/findByBookId/{id}")
    public User getUserByBookId(@PathVariable int id) {
        return userService.findUserByBookId(id);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleException(RuntimeException exception) {
        String details = exception.getMessage();

        return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
    }
}
