package ua.com.simplelibrary.library.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ua.com.simplelibrary.library.model.Book;
import ua.com.simplelibrary.library.service.BookServiceImpl;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/books", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class BookController {

    private BookServiceImpl bookService;

    @PostMapping
    public Book addBook(Map<String, String> map) {
        return bookService.addBook(map.get("name"));
    }

    @GetMapping
    public List<Book> getAllBooks() {
        return bookService.getAll();
    }

    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable int id) {
        bookService.deleteById(id);
    }

    @PutMapping
    public Book updateBook(Map<String, String> map) {
        return bookService.updateBook(
                Integer.parseInt(map.get("id")), map.get("name"));
    }
}
