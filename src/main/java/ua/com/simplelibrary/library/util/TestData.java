package ua.com.simplelibrary.library.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ua.com.simplelibrary.library.form.TakeForm;
import ua.com.simplelibrary.library.form.UserForm;
import ua.com.simplelibrary.library.service.BookServiceImpl;
import ua.com.simplelibrary.library.service.UserServiceImpl;

@Component
public class TestData {

    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private BookServiceImpl bookService;

    @EventListener(ApplicationReadyEvent.class)
    public void addTestData() {
        userService.addUser(new UserForm("username 1", "Oleksandr", "Oleksandrenko"));
        userService.addUser(new UserForm("username 2", "Pavlo", "Pavlenko"));
        userService.addUser(new UserForm("username 3", "Vasyl", "Vasylenko"));

        bookService.addBook("math");
        bookService.addBook("physics");
        bookService.addBook("poems");
        bookService.addBook("history");
        bookService.addBook("novels");
        bookService.addBook("art");

        userService.takeBook(new TakeForm("username 3", "math"));
        userService.takeBook(new TakeForm("username 3", "art"));
        userService.takeBook(new TakeForm("username 1", "poems"));
    }
}
