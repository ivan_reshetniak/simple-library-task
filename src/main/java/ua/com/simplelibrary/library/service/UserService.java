package ua.com.simplelibrary.library.service;

import ua.com.simplelibrary.library.form.TakeForm;
import ua.com.simplelibrary.library.form.UserForm;
import ua.com.simplelibrary.library.model.User;

import java.util.List;

public interface UserService {

    User addUser(UserForm form);

    List<User> getAll();

    void deleteUserById(int id);

    User updateUser(int id ,UserForm form);

    User findUserByUsername(String username);

    User takeBook(TakeForm form);

    User returnBook(TakeForm form);

    User findUserByBookId(int id);
}
