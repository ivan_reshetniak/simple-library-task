package ua.com.simplelibrary.library.service;

import ua.com.simplelibrary.library.model.Book;

import java.util.List;

public interface BookService {

    Book addBook(String name);

    List<Book> getAll();

    void deleteById(int id);

    Book updateBook(int id, String name);

    Book getBookByName(String name);
}
