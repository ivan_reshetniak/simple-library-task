package ua.com.simplelibrary.library.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.simplelibrary.library.model.Book;
import ua.com.simplelibrary.library.repository.BookRepository;
import ua.com.simplelibrary.library.repository.UserRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class BookServiceImpl implements BookService {

    private BookRepository repository;
    private UserRepository userRepository;

    @Transactional
    public Book addBook(String name) {
        Book book = new Book();
        book.setName(name);
        book.setTaken(false);

        repository.save(book);
        return book;
    }

    @Transactional(readOnly = true)
    public List<Book> getAll() {
        return repository.findAll();
    }

    @Transactional
    public void deleteById(int id) {
        repository.deleteById((long) id);
    }

    @Transactional
    public Book updateBook(int id, String name) {
        Book book = repository.findById((long) id)
                .orElseThrow(() -> new RuntimeException("Book not found"));

        book.setName(name);
        return book;
    }

    @Transactional(readOnly = true)
    public Book getBookByName(String name) {
        Book book = repository.findBookByName(name)
                .orElseThrow(() -> new RuntimeException("Book not found"));

        return book;
    }
}
