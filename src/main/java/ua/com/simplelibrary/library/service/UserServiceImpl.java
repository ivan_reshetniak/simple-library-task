package ua.com.simplelibrary.library.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.simplelibrary.library.form.TakeForm;
import ua.com.simplelibrary.library.form.UserForm;
import ua.com.simplelibrary.library.model.Book;
import ua.com.simplelibrary.library.model.User;
import ua.com.simplelibrary.library.repository.UserRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UserRepository repository;
    private BookServiceImpl bookService;

    @Transactional
    public User addUser(UserForm form) {
        User user = new User();
        user.setUsername(form.getUsername());
        user.setLastName(form.getLastName());
        user.setFirstName(form.getFirstName());

        repository.save(user);
        return user;
    }

    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public List<User> getAll() {
        return repository.findAll();
    }

    @Transactional
    public void deleteUserById(int id) {
        //books will be deleted on cascade
        repository.deleteById((long) id);
    }

    @Transactional
    public User updateUser(int id, UserForm form) {
        User user = repository.findById((long) id)
                .orElseThrow(() -> new RuntimeException("User not found"));

        user.setFirstName(form.getFirstName());
        user.setLastName(form.getLastName());
        user.setUsername(form.getUsername());

        repository.save(user);
        return user;
    }

    @Transactional(readOnly = true)
    public User findUserByUsername(String userName) {
        return repository.findUserByUsername(userName)
                .orElseThrow(() -> new RuntimeException("User not found"));
    }

    @Transactional
    public User takeBook(TakeForm form) {
        User user = findUserByUsername(form.getUsername());
        Book book = bookService.getBookByName(form.getBookName());

        if (book.isTaken()) {
            throw new RuntimeException("Book is taken by user " + book.getUser().getUsername());
        }

        book.setUser(user);
        user.getBooks().add(book);
        book.setTaken(true);
        return user;
    }

    @Transactional
    public User returnBook(TakeForm form) {
        User user = findUserByUsername(form.getUsername());
        Book book = bookService.getBookByName(form.getBookName());

        if (!user.getBooks().contains(book)) {
            throw new RuntimeException("This user does not have this book");
        }

        book.setUser(null);
        user.getBooks().remove(book);
        book.setTaken(false);
        return user;
    }

    @Transactional(readOnly = true)
    public User findUserByBookId(int id) {
        return repository.findUserByBookId(id)
                .orElseThrow(() -> new RuntimeException("User not found"));
    }
}
